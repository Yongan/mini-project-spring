create table articles(
  id int PRIMARY KEY auto_increment,
  title VARCHAR(50) NOT NULL ,
  author VARCHAR(50) NOT NULL ,
  description VARCHAR(200) NOT NULL ,
  category_id int NOT NULL ,
  create_date VARCHAR(20),
  photo_path VARCHAR(200)
);

CREATE TABLE categories (
  id int PRIMARY KEY auto_increment,
  name VARCHAR(50) NOT NULL ,
  desc VARCHAR(200) NOT NULL
);