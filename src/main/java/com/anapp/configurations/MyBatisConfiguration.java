package com.anapp.configurations;



import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

@Configuration
@MapperScan("com.anapp.repositories")
public class MyBatisConfiguration {
	
		private DataSource datasource;

		@Autowired
		public MyBatisConfiguration(DataSource dataSoruce) {
			this.datasource = dataSoruce;
		}
		
		@Bean
		public DataSourceTransactionManager dataSourceTransactionManager() {
			return new DataSourceTransactionManager(datasource);
		}
		
		@Bean
		public SqlSessionFactoryBean sqlSessionFactoryBean() {
			SqlSessionFactoryBean sql = new SqlSessionFactoryBean();
			sql.setDataSource(datasource);
			return sql;
		}
		
}
