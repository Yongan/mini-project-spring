package com.anapp.configurations;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class MyBatis {
	@Bean
	@Profile("posgresql")
	public DataSource postreSQL() {
		DriverManagerDataSource db = new DriverManagerDataSource();
		db.setDriverClassName("org.postgresql.Driver");
		db.setUrl("jdbc:postgresql://localhost:5432/ams");
		db.setUsername("postgres");
		db.setPassword("posdb");
		return db;
	}
	
	@Bean
	@Profile("h2")
	public DataSource h2DB() {
		EmbeddedDatabaseBuilder h2 = new EmbeddedDatabaseBuilder();
		h2.setType(EmbeddedDatabaseType.H2);
		h2.addScript("classpath:/db/createTable.sql");
		h2.addScript("classpath:/db/data.sql");
		return h2.build();
	}
}
