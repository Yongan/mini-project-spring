package com.anapp.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@PropertySource("classpath:/appconfig.properties")
public class ImageAccessConfiguration extends WebMvcConfigurerAdapter{
	@Value("${serverPath}")
	private String serverPath;// = "C:/Users/YONGAN/Desktop/image/";
	@Value("${clientPath}")
	private String clientPath;// = "/image/**";
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(clientPath).addResourceLocations("file:"+serverPath);
	}

}
