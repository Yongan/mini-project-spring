package com.anapp.repositories;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.anapp.models.Category;

@Repository
public interface CategoryRepository {
	@Select("SELECT c.id, c.name, c.desc FROM categories c WHERE id=#{id}")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="name",column="name"),
		@Result(property="description",column="desc")
	})
	public Category findOne(int id);
	
	@Select("SELECT c.id,  c.name, c.desc FROM categories c ORDER BY id")
	@Results({
		@Result(property="id",column="id"),
		@Result(property="name",column="name"),
		@Result(property="description",column="desc")
	})
	public List<Category> findAll();
	
	@Delete("DELETE FROM categories WHERE id=#{id}")
	public void delete(int id);
	
	@Update("UPDATE categories SET name=#{name},desc=#{description} WHERE id=#{id};")
	public void update(Category category);
	
	@Insert("INSERT INTO categories (name,desc) VALUES (#{name},#{description});")
	public void insert(Category category);
}
