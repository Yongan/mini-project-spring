package com.anapp.repositories;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.anapp.models.Article;

@Repository
public interface ArticleRepository {
	@Select("SELECT a.id,a.title,a.author,a.description,a.category_id,a.create_date,a.photo_path,"
			+ "c.name,c.desc FROM articles a INNER JOIN categories c ON a.category_id = c.id WHERE a.id=#{id}")
	@Results({
		@Result(property="description",column="description"),
		@Result(property="createDate",column="create_date"),
		@Result(property="photoPath",column="photo_path"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="name"),
		@Result(property="category.description",column="desc")
	})
	public Article findOne(int id);
	
	@Select("SELECT a.id,a.title,a.author,a.description,a.category_id,a.create_date,a.photo_path,"
			+ "c.name,c.desc FROM articles a INNER JOIN categories c ON a.category_id = c.id ORDER BY a.id limit #{start} offset #{to}")
	@Results({
		@Result(property="description",column="description"),
		@Result(property="createDate",column="create_date"),
		@Result(property="photoPath",column="photo_path"),
		@Result(property="category.id",column="category_id"),
		@Result(property="category.name",column="name"),
		@Result(property="category.description",column="desc")
	})
	public List<Article> findAll(@Param("start") int start,@Param("to")int to);
	
	@Insert("INSERT INTO articles(title,author,description,category_id,create_date,photo_path) VALUES(#{title},"
			+ "#{author},#{description},#{category.id},#{createDate},#{photoPath})")
	public void insert(Article article);
	
	@Delete("DELETE FROM articles WHERE id=#{id}")
	public void delete(int id);
	
	@Update("UPDATE articles SET title=#{title},author=#{author},description=#{description},category_id=#{category.id} WHERE id=#{id}")
	public void update(Article article);
	
	@Select("UPDATE articles SET title=#{title},author=#{author},description=#{description},category_id=#{category.id},photo_path=#{photoPath} WHERE id=#{id}")
	public void updateWithPic(Article article);
	
	@Select("Select count(*) from articles")
	public int totalRecord();
	
}
