package com.anapp.services;

import java.util.List;

import com.anapp.models.Category;

public interface CategoryService {
	public Category findOne(int id);
	public List<Category> findAll();
	public void delete(int id);
	public void update(Category category);
	public void insert(Category category);
}
