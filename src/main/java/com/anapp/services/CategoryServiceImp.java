package com.anapp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anapp.models.Category;
import com.anapp.repositories.CategoryRepository;

@Service
public class CategoryServiceImp implements CategoryService{

	private CategoryRepository cateRepo;
	
	@Autowired
	public void setCateRepo(CategoryRepository cateRepo) {
		this.cateRepo = cateRepo;
	}

	@Override
	public Category findOne(int id) {
		return cateRepo.findOne(id);
	}

	@Override
	public List<Category> findAll() {		
		return cateRepo.findAll();
	}

	@Override
	public void delete(int id) {
		cateRepo.delete(id);
	}

	@Override
	public void update(Category category) {
		cateRepo.update(category);
	}

	@Override
	public void insert(Category category) {
		cateRepo.insert(category);
	}
}
