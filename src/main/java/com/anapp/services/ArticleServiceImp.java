package com.anapp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anapp.models.Article;
import com.anapp.repositories.ArticleRepository;

@Service
public class ArticleServiceImp implements ArticleService{

	private ArticleRepository articleRepo;
	
	@Autowired
	public void setArticleRepo(ArticleRepository articleRepo) {
		this.articleRepo = articleRepo;
	}
	
	@Override
	public Article findOne(int id) {
		return articleRepo.findOne(id);
	}

	@Override
	public List<Article> findAll(int start,int to) {
		return articleRepo.findAll(start,to);
	}

	@Override
	public void insert(Article article) {
		articleRepo.insert(article);		
	}

	@Override
	public void delete(int id) {
		articleRepo.delete(id);		
	}

	@Override
	public void update(Article article) {
		articleRepo.update(article);		
	}

	@Override
	public void updateWithPic(Article article) {
		articleRepo.updateWithPic(article);		
	}

	@Override
	public int totalRecord() {
		return articleRepo.totalRecord();
	}

}
