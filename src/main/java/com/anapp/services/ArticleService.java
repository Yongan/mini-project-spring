package com.anapp.services;

import java.util.List;

import com.anapp.models.Article;


public interface ArticleService {
	public Article findOne(int id);
	public List<Article> findAll(int start, int to);
	public void insert(Article article);
	public void delete(int id);
	public void update(Article article);
	public void updateWithPic(Article article);
	public int totalRecord();
}
