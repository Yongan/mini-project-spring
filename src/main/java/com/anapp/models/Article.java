package com.anapp.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {
	private int id;
	@NotNull
	@Size(min=5,max=30)
	private String title;
	@NotNull
	@Size(min=5,max=30)
	private String author;
	@NotNull
	@Size(min=5,max=300)
	private String description;
	@NotNull
	private Category category;
	private String createDate;
	private String photoPath;
	
	public Article(){}

	public Article(int id, String title, String author, String description, Category category, String createDate,
			String photoPath) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.description = description;
		this.category = category;
		this.createDate = createDate;
		this.photoPath = photoPath;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", author=" + author + ", description=" + description
				+ ", category=" + category + ", createDate=" + createDate + ", photoPath=" + photoPath + "]";
	}
	
	
}
