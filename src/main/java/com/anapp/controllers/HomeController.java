package com.anapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	@RequestMapping("/a")
	public String add() {
		return "add-update";
	}
	@RequestMapping("/v")
	public String view() {
		return "view";
	}
}
