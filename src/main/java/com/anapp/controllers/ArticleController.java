package com.anapp.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.anapp.models.Article;
import com.anapp.services.ArticleService;
import com.anapp.services.CategoryService;

@Controller
public class ArticleController {
	
	private ArticleService articleService;
	private CategoryService cateSer;
	
	private String path = "C:/Users/YONGAN/Desktop/image";	
	
	@Autowired
	public void setArticleService(ArticleService as) {
		this.articleService = as;
	}
	
	@Autowired
	public void setCateSer(CategoryService cateSer) {
		this.cateSer = cateSer;
	}

	@GetMapping("/article")
	public String article(ModelMap map) {
		map.addAttribute("articles", articleService.findAll(6,0));
		map.addAttribute("totalPage",Math.ceil((double) articleService.totalRecord()/6.0));
		map.addAttribute("prev",0);
		map.addAttribute("next",1);
		System.out.println(articleService.totalRecord());
		System.out.println(Math.ceil((double) articleService.totalRecord()/6.0));
		return "article";
	}
	
	//for pagination
	@GetMapping("/article1")
	public String article(ModelMap map,@RequestParam("page") int page) {
		int totalPage = (int) Math.ceil((double) articleService.totalRecord()/6.0);
		map.addAttribute("articles", articleService.findAll(6,6*(page-1)));
		map.addAttribute("totalPage",totalPage);
		map.addAttribute("prev",page-1);
		map.addAttribute("next",page=page==totalPage?page:page+1);
		System.out.println(articleService.totalRecord());
		System.out.println(Math.ceil((double) articleService.totalRecord()/6.0));
		System.out.println(page);
		return "article";
	}
	
	@GetMapping("/article/add")
	public String openAddPage(ModelMap map) {
		Article article = new Article();
		map.addAttribute("article", article);
		map.addAttribute("addpage", true);
		map.addAttribute("fileEmpty",false);
		map.addAttribute("categories",cateSer.findAll());
		return "add-update";
	}

//-------- Add Section
	@PostMapping("/article/add")
	public String saveArticle(@Valid @ModelAttribute Article article,BindingResult result,ModelMap map,
			@RequestParam("file") MultipartFile file) {
		if(result.hasErrors()) {
			System.out.println(result);
			map.addAttribute("article", article);
			map.addAttribute("addpage", true);
			map.addAttribute("fileEmpty",true);
			map.addAttribute("categories",cateSer.findAll());
			System.out.println(result);
			return "add-update";
		}
		if(file.isEmpty()) {
			map.addAttribute("article", article);
			map.addAttribute("addpage", true);
			map.addAttribute("categories",cateSer.findAll());
			System.out.println("File empty error");
			return "add-update";
		}
		try {
			String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
			String filename = UUID.randomUUID().toString() + "." + extension;
			Files.copy(file.getInputStream(),Paths.get(path, filename));
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
			article.setCreateDate(sd.format(new Date()));
			article.setPhotoPath("/image/"+filename);
			article.setCategory(cateSer.findOne(article.getCategory().getId()));
			articleService.insert(article);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:/article";
	}

//------update section
	@GetMapping("article/update/{id}")
	public String openUpdatePage(ModelMap map, @PathVariable("id") int id) {
		Article article = articleService.findOne(id);
		map.addAttribute("article", article);
		map.addAttribute("addpage", false);
		map.addAttribute("categories",cateSer.findAll());
		map.addAttribute("fileEmpty",false);
		return "add-update";
	}
	
	@PostMapping("/article/update")
	public String updateArticle(@Valid @ModelAttribute Article article,BindingResult result,ModelMap map,
			@RequestParam("file") MultipartFile file) {
		if(result.hasErrors()) {
			System.out.println(result);
			map.addAttribute("article", article);
			map.addAttribute("addpage", false);
			map.addAttribute("fileEmpty",true);
			return "article-add-update";
		}
//		if(file.isEmpty()) {
//			map.addAttribute("article", article);
//			map.addAttribute("addpage", false);
//			return "article-add-update";
//		}
		if(!file.isEmpty()) {
			try {
				String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
				String filename = UUID.randomUUID().toString() + "." + extension;
				System.out.println(filename);
				Files.copy(file.getInputStream(),Paths.get(path, filename));
				article.setPhotoPath("/image/"+filename);
				article.setCategory(cateSer.findOne(article.getCategory().getId()));
				articleService.updateWithPic(article);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else {
			article.setCategory(cateSer.findOne(article.getCategory().getId()));
			articleService.update(article);
		}
		
		
		return "redirect:/article";
	}
	
	@GetMapping("article/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}
	
	@GetMapping("article/view/{id}")
	public String view(@PathVariable("id") int id, ModelMap map) {
		map.addAttribute("article", articleService.findOne(id));
		return "view";
		
	}
}
