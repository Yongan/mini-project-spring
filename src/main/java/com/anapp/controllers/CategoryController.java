package com.anapp.controllers;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.anapp.models.Category;
import com.anapp.services.CategoryService;

@Controller
public class CategoryController {
	
	private CategoryService cateSer;
	
	
	@Autowired
	public void setCateSer(CategoryService cateSer) {
		this.cateSer = cateSer;
	}

	@GetMapping("/category")
	public String category(ModelMap map) {
		map.addAttribute("categorys", cateSer.findAll());
		return "category";
	}

//-------- Add Section
	@GetMapping("/category/add")
	public String openAddPage(ModelMap map) {
		Category category = new Category();
		map.addAttribute("category", category);
		map.addAttribute("addpage", true);
		return "c-add-update";
	}

	@PostMapping("/category/add")
	public String savecategory(@Valid @ModelAttribute Category category,BindingResult result,ModelMap map) {
		if(result.hasErrors()) {
			System.out.println(result);
			map.addAttribute("category", category);
			map.addAttribute("addpage", true);
			System.out.println(result);
			return "c-add-update";
		}
				
		cateSer.insert(category);
		
		return "redirect:/category";
	}

//------update section
	@GetMapping("category/update/{id}")
	public String openUpdatePage(ModelMap map, @PathVariable("id") int id) {
		Category category = cateSer.findOne(id);
		map.addAttribute("category", category);
		map.addAttribute("addpage", false);
		return "c-add-update";
	}
	
	@PostMapping("/category/update")
	public String updatecategory(@Valid @ModelAttribute Category category,BindingResult result,ModelMap map) {
		if(result.hasErrors()) {
			System.out.println(result);
			map.addAttribute("category", category);
			map.addAttribute("addpage", true);
			System.out.println(result);
			return "c-add-update";
		}
				
		cateSer.update(category);
		
		return "redirect:/category";
	}
	
	@GetMapping("category/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		cateSer.delete(id);
		return "redirect:/category";
	}
	
	@GetMapping("category/view/{id}")
	public String view(@PathVariable("id") int id, ModelMap map) {
		map.addAttribute("category", cateSer.findOne(id));
		return "view";
		
	}
}
